# Docker CI Templates

## Docker Base

The docker base (`.docker-base`) template provides the base setup for docker.

> This image provides default settings for docker jobs,
> and ideally should not be used directly unless you know what you are doing.
>
> Use the templates explained below to build and publish your docker images instead.

It sets the docker image with dind[^1], configure TLS for docker,
and login to the GL registry in the before_script
The docker image defaults to `<repo name>:<branch slug>`.

The name of the image can be changed by setting the variable to `SWAPBOX_DOCKER_IMAGE_NAME`.
Otherwise, it defaults to `${CI_PROJECT_NAME}`
The default tag cannot be changed and is set to the branch slug `CI_COMMIT_REF_SLUG`.

## Docker Build

The docker build (`.docker-build`) templates builds a snapshot image.

By default, the image name will be based on the project ref (`${CI_REGISTRY_IMAGE}`),
and the tag will be *derived* from the branch name (`${CI_COMMIT_REF_SLUG}`).

The name can be customized by setting the variable `SWAPBOX_DOCKER_IMAGE`.
In this case, the docker image will be named `${CI_REGISTRY_IMAGE}/${SWAPBOX_DOCKER_IMAGE}`.

> :warning: The `CI_REGISTRY_IMAGE` prefix cannot and should not be changed.
> This is a GitLab limitation. 

The tag of the image can be changed by overwriting the variable `SWAPBOX_DOCKER_TAG`.

> It is *not recommended* to overwrite the `SWAPBOX_DOCKER_TAG`.
> The `dev` and `latest` tags should be published on the staging and production branches (see below).

The path to the `Dockerfile` defaults to the current directory (`.`).
It can be overwritten by setting the variable `SWAPBOX_DOCKERFILE_PATH`.
The value should be the path to the directory *containing* the `Dockerfile`.

If you need to pass extra arguments to `docker build`, you can define them in `SWAPBOX_DOCKER_BUILD_ARGS`.

This job runs on the `build` stage, you can set a different `stage:` value on your job.

## Docker Publish

The docker publish (`.docker-publish`) template publishes a release image ready to be used.
It will tag the `SWAPBOX_DOCKER_IMAGE` with a custom release tag (`SWAPBOX_DOCKER_RELEASE_TAG`).

Set `SWAPBOX_DOCKER_IMAGE_TAG` to the desired tag value, if you want a custom release tag.

> :warning: Using `.docker-publish` directly is *not recommended*.
> Instead, use the `.docker-publish-dev` `.docker-publish-latest` job templates described below.

This job runs on the `publish` stage, you can set a different `stage:` value on your job.

### Docker Publish `dev`

The docker publish template (`.docker-publish-dev`) tags the `SWAPBOX_DOCKER_IMAGE` with the `dev` tag.

This will only run on the `CI_DEFAULT_BRANCH` which should be the staging branch of your project.
The published image should be suitable for use in staging environments for *testing purposes*.

This job runs on the `publish` stage, you can set a different `stage:` value on your job.

### Docker Publish `latest`

The docker publish template (`.docker-publish-latest`) tags the `SWAPBOX_DOCKER_IMAGE` with the `latest` tag.

This will only run on the `SWAPBOX_PRODUCTION_BRANCH` which should be the production branch of your project.
The published image should be suitable for use in staging and production environments.

This job runs on the `publish` stage, you can set a different `stage:` value on your job.

---

[^1]: dind stands for **d**ocker **in** **d**ocker

# Pipenv

A simple image, based on `python:3.9-alpine` which provides:
  - `pipenv`

## Purpose

Run the Swapbox GUI CI tasks.
The python version of the images should match the version used by the GUI project. 

# ci-runners

Definitions of various docker images to be used for CI/CD.

## Adding A New Image

You can define a docker image in this repo to be built and used as an image for the CI/CD other projects.

Simply create a directory with:

- `Dockerfile`: definition of the docker image
- `README.md`: info on the image and its purpose

> Images are automatically built and published through CI/CD.  
> :warning: Do not manually push images!

The name of the image *should* match the name of its directory.
This is done by setting the `SWAPBOX_DOCKER_IMAGE_NAME` and `SWAPBOX_DOCKERFILE_PATH` variables to the name of the directory.

For example, the `pipenv` image located at `pipenv/Dockerfile`
will be: `registry.gitlab.com/truelevel/swapbox/ci-runners/pipenv`

> When submitting the merge request with your new image, please create and add a label for it.
> The label test should be the name of the image and directory, and the color should be "moderate blue" (`#428BCA`).

### Docker Tags

The tag is based on the name of the branch on which the image is built,
as defined by the [`$CI_COMMIT_REF_SLUG`][ci-commit-ref-slug] variable.

Constant tags:
 - Images built on the staging branch (`develop`) will be tagged with `dev`.  
   Those are meant to be *tested* locally,  within the CI of your project,
   or to deploy to the project's staging environment.
 - Images built on the production branch (`main`) will be tagged with `latest`.  
   Those are meant to be *used* for the CI/CD of your projects,
   and deployments (both staging and production).

### Procedure And Example

Take a look at the [`pipenv/ci.yml`](pipenv/ci.yml) for inspiration, your ci jobs should be similar.

1. Define variables in a template, then extend each job with the variables.
   > :warning: Globally defined variable may overwrite variables for other jobs
   > and result in failed or corrupted builds.

   ```yaml
   .shrubbery-variables:
     variables:
       SWAPBOX_DOCKER_IMAGE_NAME: shrubbery
       SWAPBOX_DOCKERFILE_PATH: shrubbery
    ```

1. Take a look at the [`ci/docker.yml` job templates](ci) for more info on how to use the docker job templates.

1. Include your local `<image_name>/ci.yml` file to the [`.gitlab-ci.yml`](.gitlab-ci.yml) file.

1. Only build & publish when necessary.

   A `rules:` section should be defined on build job(s)
   to only build when files in the `$SWAPBOX_DOCKERFILE_PATH` changed:

   ```yaml
   rules:
     - changes: [ "shrubbery/**/*" ]
   ```
   > :warning: Variable expansion does not happen in `rules:changes:`.
   > Thus the path has to be written literally.
   > The `$SWAPBOX_DOCKERFILE_PATH` cannot be used here.

   A `rules:` section should also be added on publish jobs.

   For `dev` tags, the publish job should only run
   if there were changes, and the job is on the default branch:

   ```yaml
   rules:
     - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
       changes: [ "shrubbery/**/*" ]
   ```

   For `latest` tags, the publish job should only run
   if there were changes, and the job is on the production branch (`$SWAPBOX_PRODUCTION_BRANCH`):

   ```yaml
   rules:
     - if: '$CI_COMMIT_BRANCH == $SWAPBOX_PRODUCTION_BRANCH'
       changes: [ "pipenv/**/*" ]
   ```

   > The CI already defines a `workflow:` to only run pipelines on merge requests and on a schedule.  
   > For scheduled pipelines, `rules:changes:` always return `true`.
   > Hence all images are built on schedule.

> :warning: Use `needs` on the `publish-*` jobs to only and immediately publish
> if the build job was run successfully.

## Pulling The Image

Locally, simply login to the docker registry:

```shell
docker login registry.gitlab.com -u <gitlab username> -p <gitlab access token>
```

On the CI, just specify the image:

```yaml
image: ${CI_REGISTRY}/truelevel/swapbox/ci-runners/<image name>:latest
```

> :warning: Always use `${CI_REGISTRY}` for the registry. Do not hardcode the FQDN of the registry.

It is recommended to use a variable for the tag,
such that the job can be re-run with a different one for testing. For example:

```yaml
variables:
  SWAPBOX_IMAGE_TAG: latest

image: ${CI_REGISTRY}/truelevel/swapbox/ci-runners/<image name>:${SWAPBOX_IMAGE_TAG}
```

[ci-commit-ref-slug]: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
